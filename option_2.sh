#!/bin/bash

cat /tmp/input.txt | sort -k1 -t\; -n | sed -r 's/([^;]+);([^;]+);([^;]+)/\"Tier \1\",\"\2\",\"\3\%\"/'
