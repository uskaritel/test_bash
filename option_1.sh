#!/bin/bash

declare -a FIRST_COLUMN
declare -a SECOND_COLUMN
declare -a LAST_COLUMN
declare -a ANOTHER

FILE=./input.txt

# file checking
if [ ! -f $FILE ]; then
   echo "Input file not found! Exitus"
   exit 1
fi

echo=$(awk -F ";" '{ print $1 }' $FILE | sort  > test.log)
mapfile -t FIRST_COLUMN < test.log

echo=$(awk -F ";" '{ print $2 }' $FILE | sort -r > test.log)
mapfile -t SECOND_COLUMN < test.log

echo=$(awk -F ";" '{ print $3 }' $FILE |  sort > test.log)
mapfile LAST_COLUMN < test.log


for i in "${LAST_COLUMN[@]}";

 do
     LAST=$(echo "$i" | tr -d '\r')
     ANOTHER=($LAST "${ANOTHER[@]}")

 done


echo "\"Tier ${FIRST_COLUMN[0]}\",\"${SECOND_COLUMN[0]}\",\"${ANOTHER[0]}%\""
echo "\"Tier ${FIRST_COLUMN[1]}\",\"${SECOND_COLUMN[1]}\",\"${ANOTHER[1]}%\""
echo "\"Tier ${FIRST_COLUMN[2]}\",\"${SECOND_COLUMN[2]}\",\"${ANOTHER[2]}%\""
